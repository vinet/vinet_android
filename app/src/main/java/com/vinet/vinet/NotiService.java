package com.vinet.vinet;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import android.support.v4.content.LocalBroadcastManager;

@TargetApi(Build.VERSION_CODES.KITKAT)
public class NotiService extends NotificationListenerService {
    Context context;
    private static final String TAG = "NListener";

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        String pack = sbn.getPackageName();
        Bundle extras = sbn.getNotification().extras;
        String title = extras.getString("android.title");
        String extrastring = extras.toString();
        String text = extras.getCharSequence("android.text").toString();
        String bigtext = extras.getCharSequence("android.bigText").toString();

        Log.i(TAG, "NotiService: Package:\t"+pack);
        Log.i(TAG, "NotiService: Title:\t"+title);
        Log.i(TAG, "NotiService: Extra:\t"+extrastring);
        Log.i(TAG, "NotiService: Text:\t"+text);
        Log.i(TAG, "NotiService: Bigtext:\t"+bigtext);

        Intent msgrcv = new Intent("Msg");
        msgrcv.putExtra("package", pack);
        msgrcv.putExtra("title", title);
        msgrcv.putExtra("text", text);
        msgrcv.putExtra("bigtext", bigtext);

        LocalBroadcastManager.getInstance(context).sendBroadcast(msgrcv);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        String packname = sbn.getPackageName();
        Log.i(TAG, "Notification Removed: "+packname);
    }
}
